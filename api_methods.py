from bson.objectid import ObjectId

from flask import Flask
from flask_pymongo import PyMongo
from flask import jsonify
from flask import request
import json
from bson import json_util

app = Flask('project2')
app.config['MONGO_DBNAME'] = 'project2'
app.config["MONGO_URI"] = "mongodb://localhost:27017/project2"

mongo = PyMongo(app)

@app.route('/test', methods=['GET'])
def get_some_incidents():
  incidents = mongo.db.incidents
  output = []
  for s in incidents.find().limit(3):
    output.append({'Status' : s['Status'], 'Creation Date' : s['Creation Date']})
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query1/2011-01-01/2011-02-01
@app.route('/query1/<starting_date>/<ending_date>', methods=['GET'])
def get_query1_results(starting_date, ending_date):
  incidents = mongo.db.incidents
  output = []
  aggregation_string=[
    {
        "$match":{
            "$and": [{
                "Creation Date": {"$gt":starting_date}
            }, {
                "Creation Date": {"$lt":ending_date}
            }]}
    },
    {
        "$group" :
        {
            "_id": "$Type of Service Request",
            "countPerType": {"$sum": 1}
        }
    },
    {
        "$sort" : {"countPerType": -1}
    }
]

  for s in incidents.aggregate(aggregation_string):
    output.append(s)
  return jsonify({'result' : output})


# Test it with
# curl -XGET 127.0.0.1:5000/query2/Graffiti%20Removal/2011-01-01/2011-02-01
@app.route('/query2/<request_type>/<starting_date>/<ending_date>', methods=['GET'])
def get_query2_results(request_type, starting_date, ending_date):
  incidents = mongo.db.incidents
  output = []
  aggregation_string= [
    {
        "$match":{
            "$and": [{
                # "Type of Service Request": "Graffiti Removal"
                "Type of Service Request": request_type
            }, {
                # "Creation Date": {"$gt":"2012-01-01T00:00:00"}
                "Creation Date": {"$gt": starting_date}
            }, {
                # "Creation Date": {"$lt":"2013-01-01T00:00:00"}
                "Creation Date": {"$lt": ending_date}
            }]}
    },
    {
        "$group" :
        {
            "_id": {"$toDate": "$Creation Date"},
            "countPerDay": {"$sum": 1}
        }
    },
    {
        "$sort" : {"countPerDay": -1}
    }
]


  for s in incidents.aggregate(aggregation_string):
    output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query3/2011-01-01
@app.route('/query3/<date>', methods=['GET'])
def get_query3_results(date):
  incidents = mongo.db.incidents
  output = []
  aggregation_string= [
    {
        "$match" : {
            'Creation Date' : {"$eq" : date}
        }
    },
    {
        "$group" : {
            "_id" : {
                "ZIP Code" : "$ZIP Code",
                "Type of Service Request" : "$Type of Service Request"
            },
            "count" : { "$sum" : 1 }
        }
    },
    {
        "$sort" : {
            "count" : -1
        }
    },
    {
        "$limit" : 3
    }
]

  for s in incidents.aggregate(aggregation_string):
    output.append(s)
  return jsonify({'result' : output})


# Test it with
# curl -XGET 127.0.0.1:5000/query4/Abandoned%20Vehicle%20Complaint
@app.route('/query4/<type_of_request>', methods=['GET'])
def get_query4_results(type_of_request):
  print(type_of_request)
  wards = mongo.db.ward
  output = []
  aggregation_string=[
    {
        "$match": {
            "type of service request": type_of_request
        }
    },
    {
        "$sort" : {
            "count": 1
        }
    },
    {
        "$limit" : 3
    }
]


  for s in wards.aggregate(aggregation_string):
    s['_id'] = str(s['_id'])
    output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query5/2011-01-01/2011-10-01
@app.route('/query5/<starting_date>/<ending_date>', methods=['GET'])
def get_query5_results(starting_date, ending_date):
  incidents = mongo.db.incidents
  output = []
  aggregation_string=[
        {
        "$match":{
            "$and": [{
                "Creation Date": {"$gt": starting_date}
             }, {
                "Creation Date": {"$lt": ending_date}
             }]}
        },
        {
            "$group": {
                "_id": "null",
                "Completion Time": {
                    "$avg" : {
                        "$divide": [
                            {
                                "$subtract": [
                                    {
                                        "$dateFromString": {
                                            "dateString": "$Completion Date"
                                        }
                                    },
                                    {
                                        "$dateFromString": {
                                            "dateString": "$Creation Date"
                                        }
                                    }
                                ]
                            },
                            1000 * 60 * 60 * 24
                        ]
                    }
                }
            }
        }
    ]



  for s in incidents.aggregate(aggregation_string):
    output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query6/2011-01-01/-90/-86/41/43
@app.route('/query6/<creation_date>/<min_longitude>/<max_longitude>/<min_latitude>/<max_latitude>', methods=['GET'])
def get_query6_results(creation_date,min_longitude,max_longitude,min_latitude,max_latitude):
  incidents = mongo.db.incidents
  output = []
  aggregation_string= [
        {
            "$match" : {
                "$and" :
                    [
                        {
                            "Creation Date": {
                                "$eq" : creation_date
                            }
                        },
                        {
                            "Longitude": {
                                "$gte" : float(min_longitude)
                            }
                        },
                        {
                            "Longitude": {
                                "$lte" : float(max_longitude)
                            }
                        },
                        {
                            "Latitude": {
                                "$gte" : float(min_latitude)
                            }
                        },
                        {
                            "Latitude": {
                                "$lte" : float(max_latitude)
                            }
                        },
                    ]
            }
        },
        {
            "$group" :
            {
                "_id" : {
                    "Type of Service Request" : "$Type of Service Request",
                    "Longitude" : "$Longitude",
                    "Latitude" : "$Latitude",
                },
                "count" : {"$sum" : 1}
            }
        },
        {
            "$sort" : {"count":-1}
        },
        {
            "$limit":1
        }
    ]

  result = list(incidents.aggregate(aggregation_string))
  return jsonify({'result' : result})

# Test it with
# curl -XGET 127.0.0.1:5000/query7/2011-01-01T00:00:00
@app.route('/query7/<creation_date>', methods=['GET'])
def get_query7_results(creation_date):
  incidents = mongo.db.incidents
  output = []
  aggregation_string= [
    {
        "$match":{    
            "Creation Date":{
                "$eq" : creation_date 
            }
        }
    },
    {
        "$sort" : {"upvotes": -1}
    },
    {
        "$limit": 50
    }
]

  for s in incidents.aggregate(aggregation_string):
      s['_id'] = str(s['_id'])
      output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query8
@app.route('/query8', methods=['GET'])
def get_query8_results():
  users = mongo.db.users
  output = []
  aggregation_string= [
    {
        "$sort" : {"upvote_counter":-1, "name":1}
    },
    {
        "$limit": 50
    }
]

  for s in users.aggregate(aggregation_string):
    output.append({'name': s['name'], 'counter': s['upvote_counter']})

  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query9
@app.route('/query9', methods=['GET'])
def get_query9_results():
  users = mongo.db.users
  output = []
  aggregation_string= [
    {
        "$unwind" : "$wards"
    },
    {
        "$sortByCount" : "$name"
    },
    {
        "$limit": 50
    }
]

  for s in users.aggregate(aggregation_string):
    s['_id'] = str(s['_id'])
    output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query10
@app.route('/query10', methods=['GET'])
def get_query10_results():
  incidents = mongo.db.incidents
  output = []
  aggregation_string= [ {"$match" : { "has_multiple_users_with_same_telephones": True }}  ]

  for s in incidents.aggregate(aggregation_string):
    s['_id'] = str(s['_id'])
    output.append(s)
  return jsonify({'result' : output})

# Test it with
# curl -XGET 127.0.0.1:5000/query11/Jessica%20Sims
@app.route('/query11/<name>', methods=['GET'])
def get_query11_results(name):
  users = mongo.db.users
  output = []
  aggregation_string= [ {"$match" : { "name": name }}  ]

  result = list(users.aggregate(aggregation_string))
  if bool(result):
    return jsonify({'result' : result[0]['wards']})
  else:
    return "empty"

@app.route('/new_upvote/<name>/<telephone>/<incident_id>', methods=['POST'])
def new_upvote(name, telephone, incident_id):
  incidents = mongo.db.incidents
  users = mongo.db.users
  incident_cursor = incidents.aggregate( [ { "$match" : {"_id": ObjectId(incident_id)} } ] )
  incident_list = list(incident_cursor)
  incident = incident_list[0]

  print(incident)
  print(bool(incident))

  if not bool(incident):
      print("Incident not found")
      return "Indicent not found."

  # Assert same user does not upvote twice
  res = users.find_one({"name":name, "upvotes": ObjectId(incident_id)})
  if bool(res):
      print("User has already upvoted this incident. Skipping...")
      return "User has already upvoted this incident. Skipping..."
  else:
      print("User has not upvoted this incident. Advancing ...")

  incidents.update_one( { '_id' : ObjectId(incident_id) }, { '$inc': {'upvotes': 1 } } )
  res = incidents.update_one( { '_id' : ObjectId(incident_id) }, { '$addToSet' : { "telephones" : telephone } } )
  # Check if the user has same telephone as another's
  if res.modified_count == 0 :
      incidents.update_one( { '_id' : ObjectId(incident_id) }, { '$set': {'has_multiple_users_with_same_telephones' : True } } )

  ward = incident["Ward"]
  res = users.update_one( { 'name' : name }, { '$addToSet' : { "upvotes" : ObjectId(incident['_id'])} }, upsert=True )
  res = users.update_one( { 'name' : name }, { '$addToSet' : { "wards" : ward } }, upsert=True )
  res = users.update_one( { 'name' : name }, { '$inc': {'upvote_counter': 1 } } )

  return jsonify({'result' : 'OK'})

@app.route('/new_incident', methods=['POST'])
def new_incident():
  print(request.is_json)
  document=request.get_json()
  print(document)
  incidents=mongo.db.incidents
  wards=mongo.db.ward

  incidents.insert_one(document)
  wards.update_one( { 'type of service request' : document['Type of Service Request'], 'ward' : document['Ward'] }, { '$inc' : { "count" : 1 } }, upsert=True )
  return jsonify({'result' : 'OK'})

if __name__ == '__main__':
    app.run(debug=True)
