import re
import random

# pip install Faker
from faker import Faker

# pip install bson
from bson.objectid import ObjectId

# pip install pymongo
import pymongo

# pip install tqdm
import tqdm
from tqdm import tqdm

def append_unique(item, array):
    if item not in array:
        array.append(item)
        return True
    return False

client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["project2"]
incidents_collection = database["incidents"]
user_collection = database["users"]

incidents_number = incidents_collection.count_documents( {} )
max_number_of_upvotes_per_user = 1000
number_of_users = int(incidents_number / (max_number_of_upvotes_per_user/2)/2)

msg = "Provide number of users (default: " + str(number_of_users) + ")\n"
user_input = input(msg)

if user_input:
   number_of_users = int(user_input)

# Init faker
fake = Faker()
fake.seed(11235)

names=[]
while len(names) < number_of_users:
    name=fake.name()
    append_unique(name, names)

# Populate users collection
for i in tqdm(range(number_of_users)):

    raw_telephone=fake.phone_number()
    long_telephone=re.sub('[^0-9]','', raw_telephone)
    telephone=long_telephone[:10]

    address=fake.address()

    number_of_upvotes_per_user = random.randint(1, max_number_of_upvotes_per_user)
    incidents_cursor = incidents_collection.aggregate( [ { '$sample' : { 'size' : number_of_upvotes_per_user } } ] )
    incidents_list = list(incidents_cursor)

    incident_ids = []
    wards = []

    for incident in range(len(incidents_list)):
        incident_id = incidents_list[incident]["_id"]

        if append_unique(incident_id, incident_ids):
#           Assert same user does not upvote twice
#            r = user_collection.find_one({"name":name, "upvotes": incident_id})
#            if bool(r):
#                print("User has already upvoded incident. Skipping...")
#                continue

            incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$inc': {'upvotes': 1 } } )

            u = incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$addToSet' : { "telephones" : telephone } } )
            if u.modified_count == 0 :
                incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$set': {'has_multiple_users_with_same_telephones' : True } } )

            ward = incidents_list[incident]["Ward"]
            append_unique(ward, wards)

    if len(incident_ids) > 0:
        document = { "name": names[i], "telephone": telephone, "address": address, "upvotes" : incident_ids, "upvote_counter": len(incident_ids), "wards" : wards }
        d = user_collection.insert_one(document)

    # print(document)

issued_incidents_number = incidents_collection.count_documents( { "$and" : [ { "upvotes" : { "$exists" : "true" } }, { "upvotes" : { "$gte" : 1 } } ] })
print("There are ", issued_incidents_number, " issued incidents ( total:" , incidents_number, ")")
print("Percentage:", issued_incidents_number / incidents_number * 100,"%")
