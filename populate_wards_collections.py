# pip install pymongo
import pymongo

client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["project2"]
incidents_collection = database["incidents"]
ward_collection = database["ward"]

incidents_number = incidents_collection.count_documents( {} )

i=0
for incident in incidents_collection.find():
    i = i + 1
    ward_collection.update_one( { 'type of service request' : incident['Type of Service Request'], 'ward' : incident['Ward'] }, { '$inc' : { "count" : 1 } }, upsert=True )
    print(i, "/", incidents_number)
