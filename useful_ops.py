import sys
import pymongo
import json
from bson.objectid import ObjectId

# Help dialog
def help():
    print("Usage:")
    print("    $ python " + sys.argv[0] + " -h")
    print("    > Prints this dialog.")
    print("")
    print("    $ python " + sys.argv[0] + " init_incidents")
    print("    > Displays the command to run in your bash")
    print("")
    print("    $ python " + sys.argv[0] + " remove_upvotes")
    print("    > Set all \"upvotes\" fields to 0 in \"incidents\" collection and drops \"users\" collection.")
    print("")
    print("    $ python " + sys.argv[0] + " new_upvote")
    print("    > Safely creates a new for a given name, telephone and incident ID.")
    print("")
    print("    $ python " + sys.argv[0] + " new_incident" + "incident json string")
    print("    > Adds the JSON provided in 'incidents' collection.")
    print("    > NOTE: The input must be a valid JSON using double quotes around fields. The entire string must be enclosed in single quotes.")
    print("    Example incident json string:")
    print("    '{\"field\":\"value\"}':")
    print("")
    print("    $ python " + sys.argv[0] + " create_indices")
    print("    > Generate indices into 'incidents' and 'users' collection.")
    print("")

# Create incidents collection from csv
def init_incidents():
    print("Run the following command:")
    print("$ for csv_name in ../dataset/*.csv ; do mongoimport -d project2 -c incidents --type csv --file /dataset/$csv_name --headerline; done")

# Initialize upvotes
def remove_upvotes():
    print("Dropping 'users' collection..")
    database.drop_collection("users")
    print(".. ok")
    print("Removing upvotes from 'incidents' collection ..")
    incidents_collection.update_many( { "upvotes" : {"$exists" : "true"} }, { '$unset': {'upvotes': 0 } } )
    print(".. ok")
    print("Removing telephones from 'incidents' collection ..")
    incidents_collection.update_many( { "telephones" : {"$exists" : "true"} }, { '$unset': {'telephones': [] } } )
    incidents_collection.update_many( { "has_multiple_users_with_same_telephones" : {"$exists" : "true"} }, { '$unset': {'has_multiple_users_with_same_telephones': False } } )
    print(".. ok")
    print("Dropping indices in 'incidents' collection ..")
    incidents_collection.drop_indexes()
    print(".. ok")

def retrieve_argument(field_name, default):
    msg="Give a " + field_name + " (default is " + default + ")\n"
    user_input=input(msg)
    if bool(user_input):
        return user_input
    return default

def new_upvote():
    # default args
    name="Vrasidas Mpizmpikis"
    telephone="11235813"
    address="unknown"
    incident_id="5c419f0ac9e0a42a0c18bd55"

    name=retrieve_argument("name", name)
    telephone=retrieve_argument("telephone", telephone)
    address=retrieve_argument("address", address)
    incident_id=retrieve_argument("incident_id", incident_id)

    incident_cursor = incidents_collection.aggregate( [ { "$match" : {"_id": ObjectId(incident_id)} } ] )
    incident_list = list(incident_cursor)
    incident = incident_list[0]

    print(incident)
    print(bool(incident))

    if not bool(incident):
        print("Incident not found")

    # Assert same user does not upvote twice
    res = user_collection.find_one({"name":name, "upvotes": ObjectId(incident_id)})
    print(res)
    if bool(res):
        print("User has already upvoted this incident. Skipping...")
        return False
    else:
        print("User has not upvoted this incident. Advancing ...")

    incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$inc': {'upvotes': 1 } } )
    res = incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$addToSet' : { "telephones" : telephone } } )
    # Check if the user has same telephone as another's
    if res.modified_count == 0 :
        incidents_collection.update_one( { '_id' : ObjectId(incident_id) }, { '$set': {'has_multiple_users_with_same_telephones' : True } } )

    ward = incident["Ward"]
    res = user_collection.update_one( { 'name' : name }, { '$addToSet' : { "upvotes" : ObjectId(incident['_id'])} }, upsert=True )
    res = user_collection.update_one( { 'name' : name }, { '$addToSet' : { "wards" : ward } }, upsert=True )
    if address!="unknown":
        res = user_collection.update_one( { 'name' : name }, { '$set' : { "address" : address } })
    res = user_collection.update_one( { 'name' : name }, { '$inc': {'upvote_counter': 1 } } )

def new_incident(document):
    incidents_collection.insert_one(json.loads(document))
    ward_collection.update_one( { 'type of service request' : document['Type of Service Request'], 'ward' : document['Ward'] }, { '$inc' : { "count" : 1 } }, upsert=True )

def create_indices():
    print("Creating index in collection 'incidents' in field 'Creation Date'..")
    incidents_collection.create_index([("Creation Date", pymongo.ASCENDING)])
    print("..ok")

    print("Creating index in collection 'incidents' in field 'has_multiple_users_with_same_telephones'..")
    incidents_collection.create_index([("has_multiple_users_with_same_telephones", pymongo.ASCENDING)])
    print("..ok")

    print("Creating index in collection 'incidents' in field 'upvotes'..")
    incidents_collection.create_index([("Creation Date", pymongo.ASCENDING), ("upvotes", pymongo.DESCENDING)])
    print("..ok")

    print("Creating index in collection 'users' in field 'upvote_counter'..")
    user_collection.create_index([("upvote_counter", pymongo.ASCENDING)])
    print("..ok")

# Database things
client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["project2"]
incidents_collection = database["incidents"]
user_collection = database["users"]
ward_collection = database["ward"]

if len(sys.argv) == 1  or sys.argv[1] == "-h" or sys.argv[1] == "help":
    help()
    exit()

if sys.argv[1] == "init_incidents":
    init_incidents()
elif sys.argv[1] == "remove_upvotes":
    remove_upvotes()
elif sys.argv[1] == "new_upvote":
    new_upvote()
elif sys.argv[1] == "new_incident":
    if len(sys.argv) != 3:
        print("Expected a document")
        help()
    else:
        new_incident(sys.argv[2])
elif sys.argv[1] == "create_indices":
    create_indices()

else:
    print ("Argument unknown")

